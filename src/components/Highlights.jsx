import React from 'react';
import {
    Button,
    Card, 
    Col, 
    Container, 
    Row,
} from 'react-bootstrap';
import Cards from './Card';
import CourseCard from './CourseCard';

const Highlights = () => {
    const image1 = "https://www.oberlo.com/media/1603970279-pexels-photo-3.jpg?fit=max&fm=jpg&w=1824";
    const image2 = "https://archive.org/download/06-07-2016_Images_Images_page_1/02_PT_hero_42_153645159.jpg";
    const image3 = "https://images.unsplash.com/photo-1612151855475-877969f4a6cc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8aGQlMjBpbWFnZXxlbnwwfHwwfHw%3D&w=1000&q=80";
    const lorem = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi est ipsa eos iusto, architecto numquam quidem quos, iste, necessitatibus ea assumenda quia porro ut ducimus dolorem animi quaerat dolorum reprehenderit!"
    return (
        <Container fluid className="mx-auto">
            <Row className="m-3">
                <Cards title="Title 1" desc={lorem} image={image1}/>
                <Cards title="Title 2" desc={lorem} image={image2}/>
                <Cards title="Title 3" desc={lorem} image={image3}/>
            </Row>
        </Container>
    )
}

export default Highlights;
