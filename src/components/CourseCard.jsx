import React from 'react'

// react-bootstrap component
import { Button, Card } from 'react-bootstrap';

const CourseCard = ({title, desc, price}) => {
    const thousands_separators = (num) => {
        let num_parts = num.toString().split(".");
        num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return num_parts.join(".");
    }
    return (
        <Card className="text-left" style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Subtitle className="mb-2 text-dark">
                    Description: 
                    <p> {desc} </p>
                </Card.Subtitle>
                <Card.Text>
                    Price:
                    <Card.Text>PHP {thousands_separators(price)}</Card.Text>
                </Card.Text>
                <Button>Enroll</Button>
            </Card.Body>
        </Card>
    )
}

export default CourseCard
