import React from 'react';
import { Container } from 'react-bootstrap';
// CSS
import './App.css';
// Components
import Navbar from './components/Navbar';
import Home from './pages/Home';


function App() {
  return (
    <div className="App">
      <Navbar />
      <Container>
        <Home />  
      </Container>
    </div>
  );
}

export default App;