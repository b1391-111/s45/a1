import React from 'react';

// Components
import Banner from '../components/Banner';
import CourseCard from '../components/CourseCard';
import Highlights from '../components/Highlights';

const Home = () => {
    return (
        <>
            <Banner />
            <Highlights />
            <CourseCard 
                title="Sample Course"
                desc="This is a sample course offering"
                price={40000}
            />
        </>
    )
}

export default Home;
